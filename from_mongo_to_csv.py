import csv
from pymongo import MongoClient


settings = {
    "FILENAME": "ya_db_beton.csv",
    "columns": ["title", "url", "phone", "address_short"],
}

NO_SAVE_LIST = ('Узбекистан', 'Казахстан', 'Украина', 'Беларусь', 'Турция', 'Грузия', 'Азербайджан', 'Армения',
                'Киргизия', 'Туркменистан', 'Таджикистан', 'Китай', 'Монголия', 'Япония', 'Латвия', 'Литва',
                'Эстония', 'Финляндия', 'Норвегия', 'Молдова', 'Україна')


class SaveMongoDBToCSV():
    def __init__(self, *args, **kwargs):
        mongo = MongoClient('127.0.0.1', 27017, connect=False)
        db = mongo.companies_db
        self.company = db.ya_companies

    def __get_company_from_mongo(self):
        return self.company.find()

    def __del_not_used_parametrs(self, company):
        not_used_parametrs = ("ya_link", "address", "_id")
        for key in not_used_parametrs:
            company.pop(key, None)
        return company

    def __check_company_by_no_save_list(self, address_company):
        check = True
        for no_save_country in NO_SAVE_LIST:
            if no_save_country in address_company:
                check = False
        return check

    def __filtered_companies(self):
        companies = self.__get_company_from_mongo()
        for company in companies:
            fix_company = {}
            address_company = company.get("address_short")
            fix_company = self.__del_not_used_parametrs(company)
            if address_company and self.__check_company_by_no_save_list(address_company):
                yield fix_company
            elif not address_company:
                yield fix_company

    def __del_coordinate(self):
        for num in range(6):
            self.company.remove({"last_point_{}".format(num): True})

    def _save_company_to_csv(self):
        self.__del_coordinate()
        with open(settings['FILENAME'], "w", newline="") as file:
            writer = csv.DictWriter(file, fieldnames=settings['columns'])
            writer.writeheader()
            companies = self.__filtered_companies()
            for company in companies:
                # запись одной строки
                writer.writerow(company)


if __name__ == "__main__":
    save_comp = SaveMongoDBToCSV()
    save_comp._save_company_to_csv()
